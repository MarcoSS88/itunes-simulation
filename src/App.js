import React, { useState } from 'react';
import './style/app.css';
import LeftBar from './components/LeftBar';
import MainPanel from './components/MainPanel';

function App() {

  const [search, setSearch] = useState(null);
  const [query, setQuery] = useState('');
  const [playListsCreated, setPlayListsCreated] = useState([]);

  const insertQuery = (e) => {
    const inputValue = e.target.value;
    setQuery(inputValue);
  }
  const fetchSearch = async () => {
    try {
      const cors = "https://cors-anywhere.herokuapp.com/";
      const url = `https://itunes.apple.com/search?term=${query}&limit=25&entity=musicTrack`;
      const resp = await fetch(`${cors}${url}`);
      const data = await resp.json();
      setSearch(data.results);
    } catch (err) {
      console.log(err);
    }
  }

  const createNewPlaylist = () => {
    const newPlayList = [{ namePlayList: 'Nuova PlayList' }, []];
    setPlayListsCreated(newPlayList);
  }

  return (
    <div className="app">
      <LeftBar
        insertQuery={insertQuery}
        fetchSearch={fetchSearch}
        createNewPlaylist={createNewPlaylist()}
      />
      <MainPanel
        search={search}
      />
    </div>
  )
}
export default App;
