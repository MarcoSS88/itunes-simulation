import React from 'react';
import '../style/audioCmd.css';

const AudioCmd = () => {
    return (
        <div className="audioCmd">
            <span className="material-icons">fast_rewind</span>
            <span className="material-icons play">play_arrow</span>
            <span className="material-icons">fast_forward</span>
        </div>
    )
}
export default AudioCmd;
