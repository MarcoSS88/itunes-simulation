import React from 'react';
import '../style/leftBar.css';
import InputSection from './InputSection';
import Library from './Library';
import Playlist from './Playlist';

const LeftBar = ({ insertQuery, fetchSearch, createNewPlaylist }) => {
    return (
        <div className="leftBar">
            <InputSection
                insertQuery={insertQuery}
                fetchSearch={fetchSearch}
            />
            <Library />
            <Playlist
                createNewPlaylist={createNewPlaylist}
            />
        </div>
    )
}
export default LeftBar;
