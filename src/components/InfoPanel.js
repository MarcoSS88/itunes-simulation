import React from 'react';
import '../style/infoPanel.css';
import AlbumTrackList from './AlbumTrackList';

const InfoPanel = ({ search }) => {
    return (
        <div className="infoPanel">
            <AlbumTrackList
                search={search}
            />
        </div>
    )
}
export default InfoPanel;
