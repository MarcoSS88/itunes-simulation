import React, { useState } from 'react';
import '../style/mainPanel.css';
import TopBar from './TopBar';
import InfoPanel from './InfoPanel';

const MainPanel = ({ search }) => {

    const [preview, setPreviewClicked] = useState(null);

    return (
        <div className="mainPanel">
            <TopBar
                search={search}
            />
            <InfoPanel
                search={search}
            />
        </div>
    )
}
export default MainPanel;
