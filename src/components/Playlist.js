import React, { useState } from 'react';
import '../style/playlist.css';

const Playlist = ({ createNewPlaylist }) => {

    const [query, setQuery] = useState('');
    const [createNewOne, setCreateNewOne] = useState(false);

    const insertQuery = (e) => {
        const inputValue = e.target.value;
        setQuery(inputValue);
    }
    const changeStatus = () => {
        setCreateNewOne(!createNewOne);
    }
    const showInput = () => {
        return createNewOne ? 'flex' : 'none';
    }
    const hideText = () => {
        return createNewOne ? 'none' : 'flex';
    }
    // const createBar = () => {
    //     return ( 

    //     )
    // }

    return (
        <div className="playlist">
            <div>
                <div>
                    <span className="material-icons" onClick={() => changeStatus()}>library_music</span>
                </div>
                <div style={{ display: hideText() }}>
                    <p onClick={() => changeStatus()}>Create a new Playlist...</p>
                </div>
                <div className="input-button-container" style={{ display: showInput() }}>
                    <input onChange={(e) => insertQuery(e)} type="text" />
                    <button onClick={() => createNewPlaylist()}>create</button>
                </div>
            </div>
        </div>
    )
}
export default Playlist;
