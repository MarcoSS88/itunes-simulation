import React, { useState } from 'react';
import '../style/library.css';

const Library = () => {

    const [sections] = useState([
        {
            title: 'Artists',
            icon: 'library_music'
        },
        {
            title: 'Albums',
            icon: 'queue_music'
        }
        ,
        {
            title: 'Songs',
            icon: 'audiotrack'
        }
    ]);

    const showSections = () => {
        return sections.map((el, index) => {
            return (
                <div key={index}>
                    <span className="material-icons">{el.icon}</span>
                    <p>{el.title}</p>
                </div>
            )
        })
    }

    return (
        <div className="library">
            <div>Library</div>
            {showSections()}
        </div>
    )
}
export default Library;
