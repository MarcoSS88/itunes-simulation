import React from 'react';
import '../style/inputSection.css';

const InputSection = ({ insertQuery, fetchSearch }) => {

    return (
        <div className="inputSection">
            <div className="inputIcon">
                <span onClick={() => fetchSearch()} className="material-icons">search</span>
                <input className="inp" onChange={(e) => insertQuery(e)} type="text" placeholder="Search..." />
            </div>
        </div>
    )
}
export default InputSection;
