import React from 'react';
import '../style/topBar.css';
import AudioCmd from './AudioCmd';
import PlayingScreen from './PlayingScreen';
import Volume from './Volume';

const TopBar = ({ search }) => {
    return (
        <div className="topBar">
            <AudioCmd />
            <PlayingScreen
                search={search}
            />
            <Volume />
        </div>
    )
}
export default TopBar;
