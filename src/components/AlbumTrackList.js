import React from 'react';
import '../style/albumtrackList.css'

const AlbumTrackList = ({ search }) => {

    const showArtistName = () => {
        if (search !== null) {
            const oneObj = search.find(el => el.artistName === el.artistName);
            const nameArtist = oneObj.artistName;
            console.log('search:', search);
            return nameArtist
        }
    }
    const showNumTracks = () => {
        if (search !== null) {
            return search.length + ' tracks,';
        }
    }
    const showCover = () => {
        if (search !== null) {
            const oneObj = search.find(el => el.artworkUrl100 === el.artworkUrl100);
            const cover = oneObj.artworkUrl100;
            return cover
        }
    }
    const showGenre = () => {
        if (search !== null) {
            const oneObj = search.find(el => el.primaryGenreName === el.primaryGenreName);
            const genre = oneObj.primaryGenreName;
            return genre
        }
    }
    const playSound = (url) => {
        const preview = new Audio(url);
        preview.play();
    }
    const showTracks = () => {
        if (search !== null) {
            return search.map((el, index) => {
                return (
                    <div key={index}>
                        <div className="mini-cover">
                            <img src={el.artworkUrl30
                            } alt="album-cover" />
                        </div>
                        <div className="trackTitle">
                            <p>{el.trackName}</p>
                        </div>
                        <div className="albumTitle">
                            <p>{el.collectionName}</p>
                        </div>
                        <div className="preview">
                            <button onClick={() => playSound(el.previewUrl)}>preview</button>
                        </div>
                    </div>
                )
            })
        }
    }

    return (
        <div className="albumtrackList">

            <div className="title-section">
                <div>
                    <p>{showArtistName()}</p>
                </div>
                <div>
                    <p>{showNumTracks()}  {showGenre()}</p>
                </div>
            </div>

            <div className="list">
                <div className="left">
                    <div className="cover">
                        <img src={showCover()} alt="album-cover" />
                    </div>
                </div>
                <div className="track-list">
                    {showTracks()}
                </div>
            </div>
        </div>
    )
}
export default AlbumTrackList;
