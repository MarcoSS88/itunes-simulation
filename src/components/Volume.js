import React from 'react';
import '../style/volume.css';

const Volume = () => {
    return (
        <div className="volume">
            <span className="material-icons">volume_mute</span>
            <input type="range" />
            <span className="material-icons">volume_up</span>
        </div>
    )
}
export default Volume;
